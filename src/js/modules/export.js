/*
 * AngularJS TableTools Plugin
 *  Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 *  License: MIT
 */
!function(){
	'use strict';
	const tableTools = angular.module('tableTools.export', ['angularBS']);
	/**
	 * @ngdoc directive
	 * @name ttExport
	 */
	tableTools.directive('ttExport', ['$document', function($document){
		const copyElement = angular.element(
			'<textarea style="position:absolute;top:-1000px;left:-1000px"></textarea>'
		);
		angular.element($document[0].body).append(copyElement);
		return {
			restrict: 'AE',
			require: ['ttExport', '^tableTools'],
			templateUrl: 'src/templates/export.ng',
			link(scope, element, attrs, ctrl){
				ctrl[0].tableTools = ctrl[1];
			},
			controllerAs: 'ttExport',
			controller: ['$q', 'tableTools', function($q, tableTools){
				const ctrl = this;
				/**
				 */
				ctrl.exportTypes = tableTools.exportTypes;
				/**
				 * @type {*[]}
				 */
				ctrl.separators = [
					{lang: ',', separator: ','},
					{lang: ';', separator: ';'},
					{lang: tableTools.lang.tabulator, separator: '\t'}
				];
				/**
				 * @type {boolean}
				 */
				ctrl.modal = false;
				/**
				 * @type {boolean}
				 */
				ctrl.exporting = false;
				/**
				 * @type {{separator: string, fileName: string, columnNames: boolean}}
				 */
				ctrl.config = {
					separator: ',',
					fileName: '',
					columnNames: true
				};
				/**
				 */
				ctrl.showExport = function(){
					const headers = ctrl.tableTools.$element[0]
						.querySelectorAll('table > thead > tr:last-child > th');
					ctrl.columns = [];
					for(let h = 0; h < headers.length; h++){
						if(!angular.element(headers[h]).hasClass('ignore-export')){
							ctrl.columns.push({
								txt: headers[h].innerHTML,
								idx: h,
								exp: true
							});
						}
					}
					ctrl.config.fileName = $document[0].title;
					ctrl.modal = true;
				};
				/**
				 */
				ctrl.flipSelection = function(){
					for(let i = 0; i < ctrl.columns.length; i++){
						ctrl.columns[i].exp = !ctrl.columns[i].exp;
					}
				};
				/**
				 * @param type
				 * @param config
				 */
				ctrl.doExport = function(type, config){
					ctrl.exporting = type;
					const indexes = [],
						data = [],
						parseText = function(text){
							if(typeof config['parseText'] === 'function'){
								text = config['parseText'](text);
							}
							return text;
						},
						appendRow = function(){
							if(row.length){
								if(type === 'csv' || type === 'copy'){
									data.push(row.join(ctrl.config.separator));
								}else{
									data.push(row);
								}
								row = [];
							}
						};
					let row = [];
					// get columns to export
					for(let i = 0; i < ctrl.columns.length; i++){
						if(ctrl.columns[i].exp){
							indexes.push(ctrl.columns[i].idx);
							if(ctrl.config.columnNames){
								row.push(parseText(ctrl.columns[i].txt));
							}
						}
					}
					appendRow();
					// grab data
					const columns = ctrl.tableTools.$element[0]
						.querySelectorAll('table > tbody > tr:not(.ignore-export) > td');
					let rowId = -1;
					for(let c = 0; c < columns.length; c++){
						if(!!~indexes.indexOf(columns[c].cellIndex)){
							if(columns[c].parentNode['rowIndex'] !== rowId){
								rowId = columns[c].parentNode['rowIndex'];
								appendRow();
							}
							row.push(parseText(angular.element(columns[c]).text().trim()));
						}
					}
					appendRow();
					// export
					let exportCallback;
					switch(type){
						case 'copy':
							exportCallback = (data) => {
								copyElement.val(data.join("\n"));
								copyElement[0].focus();
								copyElement[0].select();
								document.execCommand("copy");
							};
							break;
						case 'csv':
							exportCallback = (data, config) => {
								const a = document.createElement("a"),
									item = '\ufeff' + data.join("\n"),
									blob = new Blob([item], {type: "text/csv", charset: 'utf-8'}),
									url = URL.createObjectURL(blob);
								a.setAttribute('style', "display: none");
								a.href = url;
								a.download = config.fileName + '.csv';
								document.body.appendChild(a);
								a.click();
								a.remove();
							};
							break;
						default:
							if(typeof config['callback'] === 'function'){
								exportCallback = config['callback'];
							}else{
								throw new Error("No callback provided for export type: " + type);
							}
							break;
					}
					$q.when(exportCallback(data, ctrl.config)).then(() => {
						if(typeof tableTools['exportNotification'] === 'function'){
							tableTools['exportNotification'](type);
						}
						ctrl.exporting = false;
						ctrl.modal = false;
					});
				};
			}]
		};
	}]);
}();