/*
 * AngularJS TableTools Plugin
 *  Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 *  License: MIT
 */
!function(){
	'use strict';
	const tableTools = angular.module('tableTools.select', []);
	/**
	 * @ngdoc factory
	 * @name ttSelect
	 */
	tableTools.factory('ttSelect', [function(){
		return function(tableTools){
			const self = this;
			self.selectAll = false;
			self.changeAll = function(){
				for(let d = 0; d < tableTools.data.length; d++){
					tableTools.data[d].ttSelected = tableTools.data[d].ttSelectable !== false
						? self.selectAll : false;
				}
			};
			self.change = function(){
				for(let d = 0; d < tableTools.data.length; d++){
					if(!tableTools.data[d].ttSelected && tableTools.data[d].ttSelectable !== false){
						self.selectAll = false;
						return;
					}
				}
				self.selectAll = true;
			};
			self.getSelected = function(){
				const selected = [];
				for(let d = 0; d < tableTools.data.length; d++){
					if(tableTools.data[d].ttSelected && tableTools.data[d].ttSelectable !== false){
						selected.push(tableTools.data[d]);
					}
				}
				return selected;
			};
			self.hasSelected = function(){
				return self.getSelected().length !== 0;
			};
		};
	}]);
	/**
	 * @ngdoc directive
	 * @name ttSelectAll
	 * @description TableTools selectAll control
	 */
	tableTools.directive('ttSelectAll', [function(){
		return {
			restrict: 'AE',
			require: '^tableTools',
			template: '<input type="checkbox" class="tt-select-all" ng-model="tableTools.ttSelect.selectAll" ' +
				'ng-change="tableTools.ttSelect.changeAll()"/>',
			replace: true
		};
	}]);
	/**
	 * @ngdoc directive
	 * @name ttSelect
	 * @description TableTools selectAll child
	 */
	tableTools.directive('ttSelect', [function(){
		return {
			restrict: 'AE',
			require: '^tableTools',
			template: '<input type="checkbox" ng-model="row.ttSelected" ng-disabled="!row.ttSelectable" ' +
				'ng-change="tableTools.ttSelect.change()"/>',
			replace: true,
			scope: {
				row: '=ttSelect'
			},
			link(scope, element, attr, tableTools){
				/**
				 * Reference to tableTools controller.
				 */
				scope.tableTools = tableTools;
				if(typeof scope.row.ttSelectable === 'undefined'){
					scope.row['ttSelectable'] = true;
				}
			}
		};
	}]);
	/**
	 * @ngdoc directive
	 * @name ttSelectedClick
	 * @description Shortcut directive for ng-disabled="!tableTools.ttSelect.hasSelected()" and ng-click="someAction(tableTools.ttSelect.getSelected())"
	 */
	tableTools.directive('ttSelectedClick', [function(){
		return {
			restrict: 'AE',
			require: '^tableTools',
			replace: true,
			scope: {
				ttSelectedClick: '<'
			},
			link(scope, element, attr, tableTools){
				scope.isDisabled = function(){
					return !tableTools.ttSelect.hasSelected();
				};
				scope.$watch('isDisabled()', function(nV){
					element.attr('disabled', nV);
				});
				element.on('click', function(){
					const selected = tableTools.ttSelect.getSelected();
					if(selected.length){
						scope.ttSelectedClick(selected);
						scope.$apply();
					}
				});
			}
		};
	}]);
}();