/*
 * AngularJS TableTools Plugin
 *  Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 *  License: MIT
 */
!function(){
	'use strict';
	const tableTools = angular.module('tableTools.search', []);
	/**
	 * @ngdoc factory
	 * @name ttSearch
	 */
	tableTools.factory('ttSearch', [function(){
		const compareWithOperator = function(variable, search, operator){
				if(typeof search === 'object'){
					for(let s in search){
						if(search.hasOwnProperty(s) && compareWithOperator(variable, search[s], operator)){
							return true;
						}
					}
					return false;
				}
				if(typeof variable === 'object'){
					for(const v in variable){
						if(
							variable.hasOwnProperty(v)
							&& v !== '$$hashKey'
							&& compareWithOperator(variable[v], search, operator)
						){
							return true;
						}
					}
					return false;
				}
				if(typeof operator === 'undefined' || operator === 'like'){
					return !!~variable.toLowerCase().indexOf(search.toLowerCase())
				}else{
					switch(operator){
						case '>':
							return variable > search;
						case '<':
							return variable < search;
						case '>=':
							return variable >= search;
						case '<=':
							return variable <= search;
						case '==':
							// noinspection EqualityComparisonWithCoercionJS
							return variable == search;
						default:
							return true;
					}
				}
			},
			hasSearchString = function(variable, search){
				if(typeof variable === 'object'){
					for(const v in variable){
						if(
							variable.hasOwnProperty(v)
							&& v !== "$$hashKey"
							&& hasSearchString(variable[v], search)
						){
							return true;
						}
					}
				}else{ // noinspection EqualityComparisonWithCoercionJS
					if(
						(typeof variable === 'number' && variable == search)
						|| (typeof variable === 'string' && !!~variable.toLowerCase().indexOf(search))
					){
						return true;
					}
				}
				return false;
			};
		return function(){
			const self = this,
				filters = {};
			/**
			 * @type {string}
			 */
			self.search = "";
			/**
			 * @param field
			 * @param controller
			 */
			self.registerFilter = function(field, controller){
				if(!(field in filters)){
					filters[field] = [];
				}
				filters[field].push(controller);
			};
			/**
			 * @param field
			 * @param controller
			 */
			self.unregisterFilter = function(field, controller){
				filters[field].splice(filters[field].indexOf(controller), 1);
				if(!filters[field].length){
					delete filters[field];
				}
			};
			/**
			 */
			self.getFiltersArray = function(){
				const result = {};
				for(const f in filters){
					if(filters.hasOwnProperty(f)){
						result[f] = [];
						for(let i = 0; i < filters[f].length; i++){
							const filter = filters[f][i],
								value = filter.getValue();
							// noinspection EqualityComparisonWithCoercionJS
							if(
								typeof value === 'undefined'
								|| value == filter.ttFilterEmpty
								|| (typeof filter.length !== 'undefined' && !filter.length)
							){ // skip empty filters
								continue;
							}
							result[f].push({
								value: value,
								operator: filter.ttFilterOperator,
								isOr: filter.ttFilterOr
							});
						}
					}
				}
				return result;
			};
			/**
			 * @param {Array} data
			 * @returns {Array}
			 */
			self.doSearch = function(data){
				if(
					!data.length
					|| (
						(
							(typeof self.search !== 'string' && typeof self.search !== 'number')
							|| self.search === ''
						)
						&& !Object.keys(filters).length
					)
				){
					return data;
				}
				const results = [],
					search = typeof self.search === 'string'
						? self.search.toLowerCase()
						: (typeof self.search === 'number' ? self.search : '');
				angular.forEach(data, (row) => {
					if(search === "" || hasSearchString(row, search)){
						for(const f in filters){
							if(filters.hasOwnProperty(f) && typeof row[f] !== 'undefined'){
								let filterResult = true,
									anyPass = false,
									orCondition = false;
								for(let i = 0; i < filters[f].length; i++){
									const filter = filters[f][i],
										value = filter.getValue();
									// noinspection EqualityComparisonWithCoercionJS
									if(
										typeof value === 'undefined'
										|| value == filter.ttFilterEmpty
										|| (typeof filter.length !== 'undefined' && !filter.length)
									){ // skip empty filters
										continue;
									}
									if(!compareWithOperator(row[f], value, filter.ttFilterOperator)){
										if(filter.ttFilterOr){
											orCondition = true;
										}else{
											filterResult = false;
											break;
										}
									}else{
										anyPass = true;
									}
								}
								if(orCondition && !anyPass){ // or filter passes if any other filter has passed
									filterResult = false;
								}
								if(!filterResult){
									return;
								}
							}
						}
						results.push(row);
					}
				});
				return results;
			};
		};
	}]);
	/**
	 * @ngdoc directive
	 * @name ttSearch
	 */
	tableTools.directive('ttSearch', [function(){
		return {
			restrict: 'AE',
			require: '^tableTools',
			template: '<div class="form-group">' +
				'<input type="text" class="form-control" ng-model="tableTools.ttSearch.search" '
				+ 'ng-change="tableTools.filterData()" placeholder="{{::tableTools.lang.search}}"/>' +
				'</div>'
		};
	}]);
	/**
	 * @ngdoc directive
	 * @param {string} ttFilter
	 * @param {string} ttFilterOperator
	 * @param {string} ttFilterEmpty
	 * @param {string} ttFilterOr
	 */
	tableTools.directive('ttFilter', [function(){
		return {
			restrict: 'A',
			require: ['ttFilter', '^tableTools', 'ngModel'],
			link(scope, element, attrs, ctrl){
				ctrl[0].tableTools = ctrl[1];
				ctrl[0].init();
			},
			bindToController: {
				ttFilter: '@',
				ttFilterOperator: '@',
				ttFilterEmpty: '@',
				ngModel: '<'
			},
			controller: ['$attrs', function($attrs){
				const ctrl = this;
				if('type' in $attrs && $attrs['type'] === 'checkbox'){
					$attrs.$observe('value', function(value){
						ctrl.value = value;
					});
				}
				/**
				 * @returns {string}
				 */
				ctrl.getValue = function(){
					if(typeof ctrl.value !== 'undefined'){
						return ctrl.ngModel ? ctrl.value : ctrl.ttFilterEmpty;
					}
					return ctrl.ngModel;
				};
				/**
				 */
				ctrl.init = function(){
					if(typeof ctrl.ttFilterOperator === 'undefined'){
						ctrl.ttFilterOperator = '==';
					}
					if(typeof ctrl.ttFilterEmpty === 'undefined'){
						ctrl.ttFilterEmpty = '';
					}
					ctrl.ttFilterOr = 'ttFilterOr' in $attrs || ('type' in $attrs && $attrs['type'] === 'checkbox');
					ctrl.tableTools.ttSearch.registerFilter(ctrl.ttFilter, ctrl);
					ctrl.tableTools.filterData();
				};
				/**
				 * @param changes
				 */
				ctrl.$onChanges = function(changes){
					if('ngModel' in changes && 'tableTools' in ctrl){
						ctrl.tableTools.filterData();
					}
				};
				/**
				 */
				ctrl.$onDestroy = function(){
					ctrl.tableTools.ttSearch.unregisterFilter(ctrl.ttFilter, ctrl);
				}
			}]
		};
	}]);
}();