/*
 * AngularJS TableTools Plugin
 *  Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 *  License: MIT
 */
!function(){
	'use strict';
	const tableTools = angular.module('tableTools');
	/**
	 * @ngdoc factory
	 * @name ttSort
	 */
	tableTools.factory('ttSort', [function(){
		const isObject = function(value){
				return value !== null && typeof value === 'object';
			},
			isNumeric = function(n){
				return !isNaN(parseFloat(n)) && isFinite(n);
			},
			compare = function(v1, v2){
				if(v1.type === v2.type){
					if(v1.type === 'string'){
						if(isNumeric(v1.value) && isNumeric(v2.value)){
							return parseFloat(v1.value) < parseFloat(v2.value) ? -1 : 1;
						}
						// Compare strings case-insensitively
						v1.value = v1.value.toLowerCase();
						v2.value = v2.value.toLowerCase();
					}else if(v1.type === 'object'){
						// For basic objects, use the position of the object
						// in the collection instead of the value
						if(isObject(v1.value)){
							v1.value = v1.index;
						}
						if(isObject(v2.value)){
							v2.value = v2.index;
						}
					}
					if(v1.value !== v2.value){
						if(typeof v1.value.localeCompare === 'function'){
							return v1.value.localeCompare(v2.value);
						}else{
							return v1.value < v2.value ? -1 : 1;
						}
					}
				}else{
					return v1.type < v2.type ? -1 : 1;
				}
			};
		return function(){
			const self = this,
				sortItems = {},
				parseOrderItem = function(orderItem, parsed){
					if(orderItem[0] === '-'){
						parsed[orderItem.substring(1)] = 'desc';
					}else{
						parsed[orderItem] = 'asc';
					}
				},
				parseOrder = function(orderValue){
					const parsed = {};
					if(typeof orderValue !== 'undefined'){
						if(typeof orderValue === 'string'){
							parseOrderItem(orderValue, parsed);
						}else if(typeof orderValue.length !== 'undefined'){
							for(let i = 0; i < orderValue.length; i++){
								parseOrderItem(orderValue[i], parsed);
							}
						}
					}
					return parsed;
				};
			let lastOrder,
				sortItemsId = 0,
				lastSortItems = 0;
			self.compareFn = compare;
			/**
			 * @param field
			 * @param controller
			 */
			self.register = function(field, controller){
				if(!(field in sortItems)){
					sortItems[field] = [];
				}
				sortItems[field].push(controller);
				sortItemsId++;
			};
			/**
			 * @param field
			 * @param controller
			 */
			self.unregister = function(field, controller){
				sortItems[field].splice(sortItems[field].indexOf(controller), 1);
				if(!sortItems[field].length){
					delete sortItems[field];

				}
				sortItemsId++;
			};
			/**
			 * @param orderValue
			 * @returns {Array}
			 */
			self.getOrder = function(orderValue){
				const order = [],
					parsed = parseOrder(orderValue);
				for(const p in parsed){
					if(parsed.hasOwnProperty(p)){
						order.push({
							col: p,
							dir: parsed[p]
						});
					}
				}
				return order;
			};
			/**
			 * Propagate order change to all child sort directives
			 * @param orderValue
			 */
			self.orderUpdate = function(orderValue){
				if(!angular.equals(orderValue, lastOrder) || lastSortItems !== sortItemsId){
					const parsed = parseOrder(orderValue);
					//
					for(let field in sortItems){
						if(sortItems.hasOwnProperty(field)){
							for(let i = 0; i < sortItems[field].length; i++){
								sortItems[field][i].updateState(
									parsed[field]
								);
							}
						}
					}
					lastOrder = angular.copy(orderValue);
					lastSortItems = sortItemsId;
					return true;
				}
				return false;
			};
		};
	}]);
	/**
	 * @ngdoc directive
	 * @name sort
	 */
	tableTools.directive('sort', [function(){
		return {
			restrict: 'A',
			require: ['^tableTools', 'sort'],
			controller: ['$element', function($element){
				/**
				 * Update sorting item class
				 * @param {string} state
				 */
				this.updateState = function(state){
					if(this.state !== state){
						if(this.state){
							$element.removeClass('sorting-' + this.state);
						}
						if(state){
							$element.addClass('sorting-' + state);
						}
						this.state = state;
					}
				};
			}],
			link(scope, element, attrs, ctrl){
				const tableTools = ctrl[0],
					sortCtrl = ctrl[1];
				//
				tableTools.ttSort.register(attrs['sort'], sortCtrl);
				scope.$on('$destroy', function(){
					tableTools.ttSort.unregister(attrs['sort'], sortCtrl);
				});
				//
				element.on('click', function(e){
					if(!e.shiftKey){ // change sorting direction
						if(tableTools.order === attrs['sort']){
							tableTools.order = '-' + attrs['sort'];
						}else{
							tableTools.order = attrs['sort'];
						}
					}else{ // append to current order array
						if(typeof tableTools.order === 'string'){
							tableTools.order = [tableTools.order];
						}else if(!Array.isArray(tableTools.order)){
							tableTools.order = [];
						}
						let found = false;
						for(let i = 0; i < tableTools.order.length; i++){
							if(tableTools.order[i] === attrs['sort']){
								tableTools.order[i] = '-' + attrs['sort'];
								found = true;
								break;
							}
							if(tableTools.order[i] === '-' + attrs['sort']){
								tableTools.order[i] = attrs['sort'];
								found = true;
								break;
							}
						}
						if(!found){
							tableTools.order.push(attrs['sort']);
						}
					}
					scope.$apply();
				});
			}
		};
	}]);
}();