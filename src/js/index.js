/*
 * AngularJS TableTools Plugin
 *  Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 *  License: MIT
 */

!function(){
	'use strict';
	const tableTools = angular.module('tableTools', [
		'tableTools.search', 'tableTools.pagination', 'tableTools.export', 'tableTools.select'
	]);
	/**
	 * @ngdoc directive
	 * @name tableTools
	 *
	 * @param {expression|Array} tableTools
	 * @param {expression|number} perPage
	 * @param {expression} perPageOptions
	 * @param {expression|number} order
	 * @param {expression|string} ttUrl
	 * @param ttResolver
	 */
	tableTools.directive('tableTools', [function(){
		const scrollTo = function(target, duration){
			const cur = window.scrollY,
				start = performance.now(),
				step = function(ts){
					const elapsed = ts - start;
					if(elapsed >= 1000){
						window.scrollTo(0, target);
						return;
					}
					window.scrollTo(0, cur - Math.sin((Math.PI / 2) / (duration / elapsed)) * (cur - target));
					window.requestAnimationFrame(step);
				};
			window.requestAnimationFrame(step);
		};
		return {
			restrict: 'A',
			scope: true,
			bindToController: {
				tableTools: '<',
				perPage: '<',
				perPageOptions: '<',
				order: '=?',
				ttUrl: '@',
				ttResolver: '<'
			},
			controllerAs: 'tableTools',
			controller: [
				'$element', '$window', '$filter', '$q', '$http', '$timeout', 'tableTools',
				'ttPagination', 'ttSearch', 'ttSort', 'ttSelect',
				function(
					$element, $window, $filter, $q, $http, $timeout, tableTools,
					ttPagination, ttSearch, ttSort, ttSelect
				){
					const ctrl = this;
					/**
					 */
					ctrl.$onInit = function(){
						ctrl.data = [];
						ctrl.dataLength = 0;
						ctrl.$element = $element;
						ctrl.lang = tableTools.lang;
						ctrl.ttSearch = new ttSearch();
						ctrl.pagination = new ttPagination();
						ctrl.ttSort = new ttSort();
						ctrl.ttSelect = new ttSelect(ctrl);
						if(typeof ctrl.perPage === 'undefined'){
							ctrl.perPage = tableTools.perPage;
						}
						if(typeof ctrl.perPageOptions === 'undefined'){
							ctrl.perPageOptions = tableTools.perPageOptions;
						}
						if(typeof ctrl.ttUrl !== 'undefined' && typeof ctrl.ttResolver !== 'function'){
							if(typeof tableTools['defaultTableToolsResolver'] === 'function'){
								ctrl.ttResolver = tableTools['defaultTableToolsResolver'];
							}else{
								ctrl.ttResolver = function(limit, offset, order, search, filters, url){
									const deferred = $q.defer();
									$http.post(url, {
										getTableToolsData: 1,
										limit: limit,
										offset: offset,
										order: order,
										search: search,
										filters: filters
									}).then(function(response){
										deferred.resolve(response.data);
									}).catch(function(){
										deferred.reject();
									});
									return deferred.promise;
								};
							}
						}
						ctrl.filterData();
					};
					/**
					 * @param changes
					 */
					ctrl.$onChanges = function(changes){
						if('tableTools' in changes){
							ctrl.filterData();
						}
					};
					/**
					 */
					ctrl.$doCheck = function(){
						if(ctrl.ttSort.orderUpdate(ctrl.order)){
							ctrl.filterData();
						}
					};
					let lastResolve = {id: 0, timeout: null};
					/**
					 */
					ctrl.filterData = function(){
						if(typeof ctrl.ttSearch === 'undefined'){ // tableTools are not yet fully initialized
							return;
						}
						let timeout;
						ctrl.loading = true;
						if(typeof ctrl.ttResolver === 'function'){
							timeout = 0;
							if(lastResolve.timeout !== null){
								$timeout.cancel(lastResolve.timeout);
								timeout = 750;
							}
							let id = ++lastResolve.id;
							lastResolve.timeout = $timeout(function(){
								ctrl.ttResolver(
									ctrl.perPage, (ctrl.pagination.page - 1) * ctrl.perPage,
									ctrl.ttSort.getOrder(ctrl.order),
									ctrl.ttSearch.search, ctrl.ttSearch.getFiltersArray(), ctrl.ttUrl
								).then(function(result){
									/** @var {{data: Array, count: number, countFiltered: number}} result */
									if(
										typeof result.data === 'undefined'
										|| typeof result.count !== 'number'
										|| typeof result.countFiltered !== 'number'
									){
										throw new Error("TableTools - wrong result format");
									}
									if(lastResolve.id === id){
										ctrl.data = result.data;
										ctrl.dataLength = result.count;
										ctrl.filteredCount = result.countFiltered;
									}
								}).catch(function(e){
									console.error(e);
									if(lastResolve.id === id){
										ctrl.data = [];
										ctrl.dataLength = 0;
										ctrl.filteredCount = 0;
									}
								}).finally(function(){
									if(lastResolve.id === id){
										ctrl.pagination.paginate(ctrl.filteredCount, ctrl.perPage);
										ctrl.ttSelect.change();
										ctrl.loading = false;
										lastResolve.timeout = null;
									}
								});
							}, timeout);
							return;
						}
						timeout = 0;
						if(lastResolve.timeout !== null){
							$timeout.cancel(lastResolve.timeout);
							timeout = 50;
						}
						lastResolve.timeout = $timeout(function(){
							ctrl.data = angular.copy(ctrl.tableTools);
							ctrl.dataLength = ctrl.data.length;
							ctrl.data = ctrl.ttSearch.doSearch(ctrl.data);
							ctrl.filteredCount = ctrl.data.length;
							ctrl.data = $filter('orderBy')(ctrl.data, ctrl.order, false, ctrl.ttSort.compareFn);
							ctrl.pagination.paginate(ctrl.data.length, ctrl.perPage);
							ctrl.data = $filter('limitTo')(ctrl.data, ctrl.perPage, ctrl.pagination.start - 1);
							ctrl.ttSelect.change();
							lastResolve.timeout = null;
							ctrl.loading = false;
						}, timeout);
					};
					/**
					 * @param {number|string} page - number of page to change to, or 'prev|next' string
					 */
					ctrl.changePage = function(page){
						const originalPage = ctrl.pagination.page;
						if(page === 'prev'){
							if(ctrl.pagination.page > 1){
								ctrl.pagination.page--;
							}
						}else if(page === 'next'){
							if(ctrl.pagination.page < ctrl.pagination.pages){
								ctrl.pagination.page++;
							}
						}else if(!isNaN(page)){
							ctrl.pagination.page = page;
						}
						if(originalPage !== ctrl.pagination.page){
							ctrl.filterData();
						}
						scrollTo(
							Math.round(
								$element[0].getBoundingClientRect().top
								+ ($window.pageYOffset || document.documentElement.scrollTop)
							) + tableTools.scrollOffset,
							1000
						);
					};
				}
			]
		}
	}]);
	/**
	 * @ngdoc directive
	 * @name ttHeader
	 * @description collection of PerPage, search, export
	 */
	tableTools.directive('ttHeader', [function(){
		return {
			restrict: 'AE',
			require: '^tableTools',
			template: '<div>' +
				'<div class="row form-inline">' +
				'<div class="col-sm-6">' +
				'<tt-per-page></tt-per-page>' +
				'<tt-loading></tt-loading>' +
				'</div>' +
				'<div class="col-sm-6 text-right">' +
				'<tt-search></tt-search>' +
				'</div>' +
				'</div>' +
				'<pagination  class="tt-pagination-top">' +
				'<div class="pull-right tt-export"><tt-export></tt-export></div>' +
				'</pagination>' +
				'</div>'
		};
	}]);
	/**
	 * @ngdoc directive
	 * @name ttLoading
	 */
	tableTools.directive('ttLoading', [function(){
		return {
			restrict: 'AE',
			require: '^tableTools',
			template: '<span ng-show="tableTools.loading">&nbsp;<i class="fa fa-spinner fa-spin fa-lg"></i></span>',
			replace: true
		};
	}]);
}();