/*
 * AngularJS TableTools Plugin
 *  Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 *  License: MIT
 */
!function(){
	'use strict';
	const app = angular.module('exampleApp', ['tableTools']);
	app.controller('exampleCtrl', ['$scope', '$http', function($scope, $http){
		$scope.exampleData = [];
		$http.get('mock_data.json').then(function(response){
			$scope.exampleData = response.data;
		});
		$scope.showSelected = function(data){
			console.log(data);
		};

		$scope.filters = {
			name: '',
			lastName: '',
			idMore: 0,
			idLess: 1001,
			male: true,
			female: true
		};
	}]);
}();