/*
 * AngularJS TableTools Plugin
 *  Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 *  License: MIT
 */

!function(){
	'use strict';
	const tableTools = angular.module('tableTools', [
		'tableTools.search', 'tableTools.pagination', 'tableTools.export', 'tableTools.select'
	]);
	/**
	 * @ngdoc directive
	 * @name tableTools
	 *
	 * @param {expression|Array} tableTools
	 * @param {expression|number} perPage
	 * @param {expression} perPageOptions
	 * @param {expression|number} order
	 * @param {expression|string} ttUrl
	 * @param ttResolver
	 */
	tableTools.directive('tableTools', [function(){
		const scrollTo = function(target, duration){
			const cur = window.scrollY,
				start = performance.now(),
				step = function(ts){
					const elapsed = ts - start;
					if(elapsed >= 1000){
						window.scrollTo(0, target);
						return;
					}
					window.scrollTo(0, cur - Math.sin((Math.PI / 2) / (duration / elapsed)) * (cur - target));
					window.requestAnimationFrame(step);
				};
			window.requestAnimationFrame(step);
		};
		return {
			restrict: 'A',
			scope: true,
			bindToController: {
				tableTools: '<',
				perPage: '<',
				perPageOptions: '<',
				order: '=?',
				ttUrl: '@',
				ttResolver: '<'
			},
			controllerAs: 'tableTools',
			controller: [
				'$element', '$window', '$filter', '$q', '$http', '$timeout', 'tableTools',
				'ttPagination', 'ttSearch', 'ttSort', 'ttSelect',
				function(
					$element, $window, $filter, $q, $http, $timeout, tableTools,
					ttPagination, ttSearch, ttSort, ttSelect
				){
					const ctrl = this;
					/**
					 */
					ctrl.$onInit = function(){
						ctrl.data = [];
						ctrl.dataLength = 0;
						ctrl.$element = $element;
						ctrl.lang = tableTools.lang;
						ctrl.ttSearch = new ttSearch();
						ctrl.pagination = new ttPagination();
						ctrl.ttSort = new ttSort();
						ctrl.ttSelect = new ttSelect(ctrl);
						if(typeof ctrl.perPage === 'undefined'){
							ctrl.perPage = tableTools.perPage;
						}
						if(typeof ctrl.perPageOptions === 'undefined'){
							ctrl.perPageOptions = tableTools.perPageOptions;
						}
						if(typeof ctrl.ttUrl !== 'undefined' && typeof ctrl.ttResolver !== 'function'){
							if(typeof tableTools['defaultTableToolsResolver'] === 'function'){
								ctrl.ttResolver = tableTools['defaultTableToolsResolver'];
							}else{
								ctrl.ttResolver = function(limit, offset, order, search, filters, url){
									const deferred = $q.defer();
									$http.post(url, {
										getTableToolsData: 1,
										limit: limit,
										offset: offset,
										order: order,
										search: search,
										filters: filters
									}).then(function(response){
										deferred.resolve(response.data);
									}).catch(function(){
										deferred.reject();
									});
									return deferred.promise;
								};
							}
						}
						ctrl.filterData();
					};
					/**
					 * @param changes
					 */
					ctrl.$onChanges = function(changes){
						if('tableTools' in changes){
							ctrl.filterData();
						}
					};
					/**
					 */
					ctrl.$doCheck = function(){
						if(ctrl.ttSort.orderUpdate(ctrl.order)){
							ctrl.filterData();
						}
					};
					let lastResolve = {id: 0, timeout: null};
					/**
					 */
					ctrl.filterData = function(){
						if(typeof ctrl.ttSearch === 'undefined'){ // tableTools are not yet fully initialized
							return;
						}
						let timeout;
						ctrl.loading = true;
						if(typeof ctrl.ttResolver === 'function'){
							timeout = 0;
							if(lastResolve.timeout !== null){
								$timeout.cancel(lastResolve.timeout);
								timeout = 750;
							}
							let id = ++lastResolve.id;
							lastResolve.timeout = $timeout(function(){
								ctrl.ttResolver(
									ctrl.perPage, (ctrl.pagination.page - 1) * ctrl.perPage,
									ctrl.ttSort.getOrder(ctrl.order),
									ctrl.ttSearch.search, ctrl.ttSearch.getFiltersArray(), ctrl.ttUrl
								).then(function(result){
									/** @var {{data: Array, count: number, countFiltered: number}} result */
									if(
										typeof result.data === 'undefined'
										|| typeof result.count !== 'number'
										|| typeof result.countFiltered !== 'number'
									){
										throw new Error("TableTools - wrong result format");
									}
									if(lastResolve.id === id){
										ctrl.data = result.data;
										ctrl.dataLength = result.count;
										ctrl.filteredCount = result.countFiltered;
									}
								}).catch(function(e){
									console.error(e);
									if(lastResolve.id === id){
										ctrl.data = [];
										ctrl.dataLength = 0;
										ctrl.filteredCount = 0;
									}
								}).finally(function(){
									if(lastResolve.id === id){
										ctrl.pagination.paginate(ctrl.filteredCount, ctrl.perPage);
										ctrl.ttSelect.change();
										ctrl.loading = false;
										lastResolve.timeout = null;
									}
								});
							}, timeout);
							return;
						}
						timeout = 0;
						if(lastResolve.timeout !== null){
							$timeout.cancel(lastResolve.timeout);
							timeout = 50;
						}
						lastResolve.timeout = $timeout(function(){
							ctrl.data = angular.copy(ctrl.tableTools);
							ctrl.dataLength = ctrl.data.length;
							ctrl.data = ctrl.ttSearch.doSearch(ctrl.data);
							ctrl.filteredCount = ctrl.data.length;
							ctrl.data = $filter('orderBy')(ctrl.data, ctrl.order, false, ctrl.ttSort.compareFn);
							ctrl.pagination.paginate(ctrl.data.length, ctrl.perPage);
							ctrl.data = $filter('limitTo')(ctrl.data, ctrl.perPage, ctrl.pagination.start - 1);
							ctrl.ttSelect.change();
							lastResolve.timeout = null;
							ctrl.loading = false;
						}, timeout);
					};
					/**
					 * @param {number|string} page - number of page to change to, or 'prev|next' string
					 */
					ctrl.changePage = function(page){
						const originalPage = ctrl.pagination.page;
						if(page === 'prev'){
							if(ctrl.pagination.page > 1){
								ctrl.pagination.page--;
							}
						}else if(page === 'next'){
							if(ctrl.pagination.page < ctrl.pagination.pages){
								ctrl.pagination.page++;
							}
						}else if(!isNaN(page)){
							ctrl.pagination.page = page;
						}
						if(originalPage !== ctrl.pagination.page){
							ctrl.filterData();
						}
						scrollTo(
							Math.round(
								$element[0].getBoundingClientRect().top
								+ ($window.pageYOffset || document.documentElement.scrollTop)
							) + tableTools.scrollOffset,
							1000
						);
					};
				}
			]
		}
	}]);
	/**
	 * @ngdoc directive
	 * @name ttHeader
	 * @description collection of PerPage, search, export
	 */
	tableTools.directive('ttHeader', [function(){
		return {
			restrict: 'AE',
			require: '^tableTools',
			template: '<div>' +
				'<div class="row form-inline">' +
				'<div class="col-sm-6">' +
				'<tt-per-page></tt-per-page>' +
				'<tt-loading></tt-loading>' +
				'</div>' +
				'<div class="col-sm-6 text-right">' +
				'<tt-search></tt-search>' +
				'</div>' +
				'</div>' +
				'<pagination  class="tt-pagination-top">' +
				'<div class="pull-right tt-export"><tt-export></tt-export></div>' +
				'</pagination>' +
				'</div>'
		};
	}]);
	/**
	 * @ngdoc directive
	 * @name ttLoading
	 */
	tableTools.directive('ttLoading', [function(){
		return {
			restrict: 'AE',
			require: '^tableTools',
			template: '<span ng-show="tableTools.loading">&nbsp;<i class="fa fa-spinner fa-spin fa-lg"></i></span>',
			replace: true
		};
	}]);
}();
/*
 * AngularJS TableTools Plugin
 *  Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 *  License: MIT
 */
angular.module('tableTools').provider('tableTools', function(){
	this.perPage = 25;
	this.perPageOptions = [
		{number: 10, text: 10},
		{number: 25, text: 25},
		{number: 50, text: 50},
		{number: 100, text: 100},
		{number: 200, text: 200},
		{number: Infinity, text: 'Wszystkie'}
	];
	this.scrollOffset = 0;
	this.lang = {
		first: 'Pierwsza strona',
		prev: 'Poprzednia strona',
		next: 'Następna strona',
		last: 'Ostatnia strona',
		results: 'Wyniki:',
		from: 'z',
		perPage: 'Wyników na stronę:',
		search: 'Szukaj...',
		filteredResults: 'Filtrowanie z:',
		export: 'Export',
		exportChooseColumns: 'Wybierz kolumny',
		flipSelection: 'odwróć zaznaczenie',
		exportColumnNames: 'Eksportuj nazwy kolumn',
		exportSeparator: 'Separator',
		tabulator: 'Tabulator',
		copy: 'Kopiuj',
		csv: 'CSV',
		copiedToClipboard: 'Skopiowano do schowka'
	};
	this.exportTypes = {
		copy: {
			lang: this.lang.copy
		},
		csv: {
			lang: this.lang.csv,
			parseText: function(txt){
				return '"' + txt.replace('"', '""') + '"';
			}
		}
	};
	this.exportNotification = function(type){
		if(type === 'copy'){
			alert(this.lang.copiedToClipboard);
		}
	};
	// noinspection JSUnusedGlobalSymbols
	this.$get = function(){
		return this;
	};
});
/*
 * AngularJS TableTools Plugin
 *  Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 *  License: MIT
 */
!function(){
	'use strict';
	const tableTools = angular.module('tableTools.export', ['angularBS']);
	/**
	 * @ngdoc directive
	 * @name ttExport
	 */
	tableTools.directive('ttExport', ['$document', function($document){
		const copyElement = angular.element(
			'<textarea style="position:absolute;top:-1000px;left:-1000px"></textarea>'
		);
		angular.element($document[0].body).append(copyElement);
		return {
			restrict: 'AE',
			require: ['ttExport', '^tableTools'],
			templateUrl: 'src/templates/export.ng',
			link(scope, element, attrs, ctrl){
				ctrl[0].tableTools = ctrl[1];
			},
			controllerAs: 'ttExport',
			controller: ['$q', 'tableTools', function($q, tableTools){
				const ctrl = this;
				/**
				 */
				ctrl.exportTypes = tableTools.exportTypes;
				/**
				 * @type {*[]}
				 */
				ctrl.separators = [
					{lang: ',', separator: ','},
					{lang: ';', separator: ';'},
					{lang: tableTools.lang.tabulator, separator: '\t'}
				];
				/**
				 * @type {boolean}
				 */
				ctrl.modal = false;
				/**
				 * @type {boolean}
				 */
				ctrl.exporting = false;
				/**
				 * @type {{separator: string, fileName: string, columnNames: boolean}}
				 */
				ctrl.config = {
					separator: ',',
					fileName: '',
					columnNames: true
				};
				/**
				 */
				ctrl.showExport = function(){
					const headers = ctrl.tableTools.$element[0]
						.querySelectorAll('table > thead > tr:last-child > th');
					ctrl.columns = [];
					for(let h = 0; h < headers.length; h++){
						if(!angular.element(headers[h]).hasClass('ignore-export')){
							ctrl.columns.push({
								txt: headers[h].innerHTML,
								idx: h,
								exp: true
							});
						}
					}
					ctrl.config.fileName = $document[0].title;
					ctrl.modal = true;
				};
				/**
				 */
				ctrl.flipSelection = function(){
					for(let i = 0; i < ctrl.columns.length; i++){
						ctrl.columns[i].exp = !ctrl.columns[i].exp;
					}
				};
				/**
				 * @param type
				 * @param config
				 */
				ctrl.doExport = function(type, config){
					ctrl.exporting = type;
					const indexes = [],
						data = [],
						parseText = function(text){
							if(typeof config['parseText'] === 'function'){
								text = config['parseText'](text);
							}
							return text;
						},
						appendRow = function(){
							if(row.length){
								if(type === 'csv' || type === 'copy'){
									data.push(row.join(ctrl.config.separator));
								}else{
									data.push(row);
								}
								row = [];
							}
						};
					let row = [];
					// get columns to export
					for(let i = 0; i < ctrl.columns.length; i++){
						if(ctrl.columns[i].exp){
							indexes.push(ctrl.columns[i].idx);
							if(ctrl.config.columnNames){
								row.push(parseText(ctrl.columns[i].txt));
							}
						}
					}
					appendRow();
					// grab data
					const columns = ctrl.tableTools.$element[0]
						.querySelectorAll('table > tbody > tr:not(.ignore-export) > td');
					let rowId = -1;
					for(let c = 0; c < columns.length; c++){
						if(!!~indexes.indexOf(columns[c].cellIndex)){
							if(columns[c].parentNode['rowIndex'] !== rowId){
								rowId = columns[c].parentNode['rowIndex'];
								appendRow();
							}
							row.push(parseText(angular.element(columns[c]).text().trim()));
						}
					}
					appendRow();
					// export
					let exportCallback;
					switch(type){
						case 'copy':
							exportCallback = (data) => {
								copyElement.val(data.join("\n"));
								copyElement[0].focus();
								copyElement[0].select();
								document.execCommand("copy");
							};
							break;
						case 'csv':
							exportCallback = (data, config) => {
								const a = document.createElement("a"),
									item = '\ufeff' + data.join("\n"),
									blob = new Blob([item], {type: "text/csv", charset: 'utf-8'}),
									url = URL.createObjectURL(blob);
								a.setAttribute('style', "display: none");
								a.href = url;
								a.download = config.fileName + '.csv';
								document.body.appendChild(a);
								a.click();
								a.remove();
							};
							break;
						default:
							if(typeof config['callback'] === 'function'){
								exportCallback = config['callback'];
							}else{
								throw new Error("No callback provided for export type: " + type);
							}
							break;
					}
					$q.when(exportCallback(data, ctrl.config)).then(() => {
						if(typeof tableTools['exportNotification'] === 'function'){
							tableTools['exportNotification'](type);
						}
						ctrl.exporting = false;
						ctrl.modal = false;
					});
				};
			}]
		};
	}]);
}();
/*
 * AngularJS TableTools Plugin
 *  Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 *  License: MIT
 */
!function(){
	'use strict';
	const tableTools = angular.module('tableTools.pagination', []);
	/**
	 * @ngdoc factory
	 * @name ttPagination
	 */
	tableTools.factory('ttPagination', [function(){
		return function(visiblePageCount){
			if(isNaN(visiblePageCount)){
				visiblePageCount = 5;
			}
			const self = this,
				pagesAround = Math.floor(visiblePageCount / 2); // visible pages around current page
			self.page = 1;
			self.pages = 1;
			self.start = 0;
			self.end = 0;
			/**
			 * @type {Array}
			 */
			self.items = [];
			/**
			 * @param {number} resultsLength
			 * @param {number} perPage
			 */
			self.paginate = function(resultsLength, perPage){
				self.pages = Math.ceil(resultsLength / perPage);
				if(self.pages === 0){
					self.pages = 1;
				}
				if(self.page > self.pages){
					self.page = self.pages;
				}
				self.items = [];
				let pagesAfter = self.pages - self.page, // number of pages after currently selected page
					i = self.page - // we set a starting page in here
						(pagesAfter < pagesAround // we won't be able to display all pages after current page
							? visiblePageCount - 1 - pagesAfter // so we display the difference before current page
							: pagesAround);
				if(i < 1){
					i = 1;
				}
				do{
					self.items.push(i);
					i++;
				}while(self.items.length < visiblePageCount && i <= self.pages);
				self.start = perPage === Infinity
					? 1
					: Math.min(
						((self.page - 1) * perPage) + 1,
						resultsLength
					);
				self.end = Math.min(self.page * perPage, resultsLength);
			};
		};
	}]);
	/**
	 * @ngdoc directive
	 * @name pagination
	 */
	tableTools.directive('pagination', [function(){
		return {
			restrict: 'AE',
			require: '^tableTools',
			transclude: true,
			templateUrl: 'src/templates/pagination.ng'
		};
	}]);
	/**
	 * @ngdoc directive
	 * @name ttPerPage
	 */
	tableTools.directive('ttPerPage', [function(){
		return {
			restrict: 'AE',
			require: '^tableTools',
			template: '<div class="form-group">' +
				'<label>{{::tableTools.lang.perPage}}&nbsp;</label>' +
				'<select class="form-control" ng-model="tableTools.perPage" ng-change="tableTools.filterData()"' +
				' ng-options="o.number as o.text for o in tableTools.perPageOptions"></select>' +
				'</div>'
		};
	}]);
}();
/*
 * AngularJS TableTools Plugin
 *  Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 *  License: MIT
 */
!function(){
	'use strict';
	const tableTools = angular.module('tableTools.search', []);
	/**
	 * @ngdoc factory
	 * @name ttSearch
	 */
	tableTools.factory('ttSearch', [function(){
		const compareWithOperator = function(variable, search, operator){
				if(typeof search === 'object'){
					for(let s in search){
						if(search.hasOwnProperty(s) && compareWithOperator(variable, search[s], operator)){
							return true;
						}
					}
					return false;
				}
				if(typeof variable === 'object'){
					for(const v in variable){
						if(
							variable.hasOwnProperty(v)
							&& v !== '$$hashKey'
							&& compareWithOperator(variable[v], search, operator)
						){
							return true;
						}
					}
					return false;
				}
				if(typeof operator === 'undefined' || operator === 'like'){
					return !!~variable.toLowerCase().indexOf(search.toLowerCase())
				}else{
					switch(operator){
						case '>':
							return variable > search;
						case '<':
							return variable < search;
						case '>=':
							return variable >= search;
						case '<=':
							return variable <= search;
						case '==':
							// noinspection EqualityComparisonWithCoercionJS
							return variable == search;
						default:
							return true;
					}
				}
			},
			hasSearchString = function(variable, search){
				if(typeof variable === 'object'){
					for(const v in variable){
						if(
							variable.hasOwnProperty(v)
							&& v !== "$$hashKey"
							&& hasSearchString(variable[v], search)
						){
							return true;
						}
					}
				}else{ // noinspection EqualityComparisonWithCoercionJS
					if(
						(typeof variable === 'number' && variable == search)
						|| (typeof variable === 'string' && !!~variable.toLowerCase().indexOf(search))
					){
						return true;
					}
				}
				return false;
			};
		return function(){
			const self = this,
				filters = {};
			/**
			 * @type {string}
			 */
			self.search = "";
			/**
			 * @param field
			 * @param controller
			 */
			self.registerFilter = function(field, controller){
				if(!(field in filters)){
					filters[field] = [];
				}
				filters[field].push(controller);
			};
			/**
			 * @param field
			 * @param controller
			 */
			self.unregisterFilter = function(field, controller){
				filters[field].splice(filters[field].indexOf(controller), 1);
				if(!filters[field].length){
					delete filters[field];
				}
			};
			/**
			 */
			self.getFiltersArray = function(){
				const result = {};
				for(const f in filters){
					if(filters.hasOwnProperty(f)){
						result[f] = [];
						for(let i = 0; i < filters[f].length; i++){
							const filter = filters[f][i],
								value = filter.getValue();
							// noinspection EqualityComparisonWithCoercionJS
							if(
								typeof value === 'undefined'
								|| value == filter.ttFilterEmpty
								|| (typeof filter.length !== 'undefined' && !filter.length)
							){ // skip empty filters
								continue;
							}
							result[f].push({
								value: value,
								operator: filter.ttFilterOperator,
								isOr: filter.ttFilterOr
							});
						}
					}
				}
				return result;
			};
			/**
			 * @param {Array} data
			 * @returns {Array}
			 */
			self.doSearch = function(data){
				if(
					!data.length
					|| (
						(
							(typeof self.search !== 'string' && typeof self.search !== 'number')
							|| self.search === ''
						)
						&& !Object.keys(filters).length
					)
				){
					return data;
				}
				const results = [],
					search = typeof self.search === 'string'
						? self.search.toLowerCase()
						: (typeof self.search === 'number' ? self.search : '');
				angular.forEach(data, (row) => {
					if(search === "" || hasSearchString(row, search)){
						for(const f in filters){
							if(filters.hasOwnProperty(f) && typeof row[f] !== 'undefined'){
								let filterResult = true,
									anyPass = false,
									orCondition = false;
								for(let i = 0; i < filters[f].length; i++){
									const filter = filters[f][i],
										value = filter.getValue();
									// noinspection EqualityComparisonWithCoercionJS
									if(
										typeof value === 'undefined'
										|| value == filter.ttFilterEmpty
										|| (typeof filter.length !== 'undefined' && !filter.length)
									){ // skip empty filters
										continue;
									}
									if(!compareWithOperator(row[f], value, filter.ttFilterOperator)){
										if(filter.ttFilterOr){
											orCondition = true;
										}else{
											filterResult = false;
											break;
										}
									}else{
										anyPass = true;
									}
								}
								if(orCondition && !anyPass){ // or filter passes if any other filter has passed
									filterResult = false;
								}
								if(!filterResult){
									return;
								}
							}
						}
						results.push(row);
					}
				});
				return results;
			};
		};
	}]);
	/**
	 * @ngdoc directive
	 * @name ttSearch
	 */
	tableTools.directive('ttSearch', [function(){
		return {
			restrict: 'AE',
			require: '^tableTools',
			template: '<div class="form-group">' +
				'<input type="text" class="form-control" ng-model="tableTools.ttSearch.search" '
				+ 'ng-change="tableTools.filterData()" placeholder="{{::tableTools.lang.search}}"/>' +
				'</div>'
		};
	}]);
	/**
	 * @ngdoc directive
	 * @param {string} ttFilter
	 * @param {string} ttFilterOperator
	 * @param {string} ttFilterEmpty
	 * @param {string} ttFilterOr
	 */
	tableTools.directive('ttFilter', [function(){
		return {
			restrict: 'A',
			require: ['ttFilter', '^tableTools', 'ngModel'],
			link(scope, element, attrs, ctrl){
				ctrl[0].tableTools = ctrl[1];
				ctrl[0].init();
			},
			bindToController: {
				ttFilter: '@',
				ttFilterOperator: '@',
				ttFilterEmpty: '@',
				ngModel: '<'
			},
			controller: ['$attrs', function($attrs){
				const ctrl = this;
				if('type' in $attrs && $attrs['type'] === 'checkbox'){
					$attrs.$observe('value', function(value){
						ctrl.value = value;
					});
				}
				/**
				 * @returns {string}
				 */
				ctrl.getValue = function(){
					if(typeof ctrl.value !== 'undefined'){
						return ctrl.ngModel ? ctrl.value : ctrl.ttFilterEmpty;
					}
					return ctrl.ngModel;
				};
				/**
				 */
				ctrl.init = function(){
					if(typeof ctrl.ttFilterOperator === 'undefined'){
						ctrl.ttFilterOperator = '==';
					}
					if(typeof ctrl.ttFilterEmpty === 'undefined'){
						ctrl.ttFilterEmpty = '';
					}
					ctrl.ttFilterOr = 'ttFilterOr' in $attrs || ('type' in $attrs && $attrs['type'] === 'checkbox');
					ctrl.tableTools.ttSearch.registerFilter(ctrl.ttFilter, ctrl);
					ctrl.tableTools.filterData();
				};
				/**
				 * @param changes
				 */
				ctrl.$onChanges = function(changes){
					if('ngModel' in changes && 'tableTools' in ctrl){
						ctrl.tableTools.filterData();
					}
				};
				/**
				 */
				ctrl.$onDestroy = function(){
					ctrl.tableTools.ttSearch.unregisterFilter(ctrl.ttFilter, ctrl);
				}
			}]
		};
	}]);
}();
/*
 * AngularJS TableTools Plugin
 *  Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 *  License: MIT
 */
!function(){
	'use strict';
	const tableTools = angular.module('tableTools.select', []);
	/**
	 * @ngdoc factory
	 * @name ttSelect
	 */
	tableTools.factory('ttSelect', [function(){
		return function(tableTools){
			const self = this;
			self.selectAll = false;
			self.changeAll = function(){
				for(let d = 0; d < tableTools.data.length; d++){
					tableTools.data[d].ttSelected = tableTools.data[d].ttSelectable !== false
						? self.selectAll : false;
				}
			};
			self.change = function(){
				for(let d = 0; d < tableTools.data.length; d++){
					if(!tableTools.data[d].ttSelected && tableTools.data[d].ttSelectable !== false){
						self.selectAll = false;
						return;
					}
				}
				self.selectAll = true;
			};
			self.getSelected = function(){
				const selected = [];
				for(let d = 0; d < tableTools.data.length; d++){
					if(tableTools.data[d].ttSelected && tableTools.data[d].ttSelectable !== false){
						selected.push(tableTools.data[d]);
					}
				}
				return selected;
			};
			self.hasSelected = function(){
				return self.getSelected().length !== 0;
			};
		};
	}]);
	/**
	 * @ngdoc directive
	 * @name ttSelectAll
	 * @description TableTools selectAll control
	 */
	tableTools.directive('ttSelectAll', [function(){
		return {
			restrict: 'AE',
			require: '^tableTools',
			template: '<input type="checkbox" class="tt-select-all" ng-model="tableTools.ttSelect.selectAll" ' +
				'ng-change="tableTools.ttSelect.changeAll()"/>',
			replace: true
		};
	}]);
	/**
	 * @ngdoc directive
	 * @name ttSelect
	 * @description TableTools selectAll child
	 */
	tableTools.directive('ttSelect', [function(){
		return {
			restrict: 'AE',
			require: '^tableTools',
			template: '<input type="checkbox" ng-model="row.ttSelected" ng-disabled="!row.ttSelectable" ' +
				'ng-change="tableTools.ttSelect.change()"/>',
			replace: true,
			scope: {
				row: '=ttSelect'
			},
			link(scope, element, attr, tableTools){
				/**
				 * Reference to tableTools controller.
				 */
				scope.tableTools = tableTools;
				if(typeof scope.row.ttSelectable === 'undefined'){
					scope.row['ttSelectable'] = true;
				}
			}
		};
	}]);
	/**
	 * @ngdoc directive
	 * @name ttSelectedClick
	 * @description Shortcut directive for ng-disabled="!tableTools.ttSelect.hasSelected()" and ng-click="someAction(tableTools.ttSelect.getSelected())"
	 */
	tableTools.directive('ttSelectedClick', [function(){
		return {
			restrict: 'AE',
			require: '^tableTools',
			replace: true,
			scope: {
				ttSelectedClick: '<'
			},
			link(scope, element, attr, tableTools){
				scope.isDisabled = function(){
					return !tableTools.ttSelect.hasSelected();
				};
				scope.$watch('isDisabled()', function(nV){
					element.attr('disabled', nV);
				});
				element.on('click', function(){
					const selected = tableTools.ttSelect.getSelected();
					if(selected.length){
						scope.ttSelectedClick(selected);
						scope.$apply();
					}
				});
			}
		};
	}]);
}();
/*
 * AngularJS TableTools Plugin
 *  Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 *  License: MIT
 */
!function(){
	'use strict';
	const tableTools = angular.module('tableTools');
	/**
	 * @ngdoc factory
	 * @name ttSort
	 */
	tableTools.factory('ttSort', [function(){
		const isObject = function(value){
				return value !== null && typeof value === 'object';
			},
			isNumeric = function(n){
				return !isNaN(parseFloat(n)) && isFinite(n);
			},
			compare = function(v1, v2){
				if(v1.type === v2.type){
					if(v1.type === 'string'){
						if(isNumeric(v1.value) && isNumeric(v2.value)){
							return parseFloat(v1.value) < parseFloat(v2.value) ? -1 : 1;
						}
						// Compare strings case-insensitively
						v1.value = v1.value.toLowerCase();
						v2.value = v2.value.toLowerCase();
					}else if(v1.type === 'object'){
						// For basic objects, use the position of the object
						// in the collection instead of the value
						if(isObject(v1.value)){
							v1.value = v1.index;
						}
						if(isObject(v2.value)){
							v2.value = v2.index;
						}
					}
					if(v1.value !== v2.value){
						if(typeof v1.value.localeCompare === 'function'){
							return v1.value.localeCompare(v2.value);
						}else{
							return v1.value < v2.value ? -1 : 1;
						}
					}
				}else{
					return v1.type < v2.type ? -1 : 1;
				}
			};
		return function(){
			const self = this,
				sortItems = {},
				parseOrderItem = function(orderItem, parsed){
					if(orderItem[0] === '-'){
						parsed[orderItem.substring(1)] = 'desc';
					}else{
						parsed[orderItem] = 'asc';
					}
				},
				parseOrder = function(orderValue){
					const parsed = {};
					if(typeof orderValue !== 'undefined'){
						if(typeof orderValue === 'string'){
							parseOrderItem(orderValue, parsed);
						}else if(typeof orderValue.length !== 'undefined'){
							for(let i = 0; i < orderValue.length; i++){
								parseOrderItem(orderValue[i], parsed);
							}
						}
					}
					return parsed;
				};
			let lastOrder,
				sortItemsId = 0,
				lastSortItems = 0;
			self.compareFn = compare;
			/**
			 * @param field
			 * @param controller
			 */
			self.register = function(field, controller){
				if(!(field in sortItems)){
					sortItems[field] = [];
				}
				sortItems[field].push(controller);
				sortItemsId++;
			};
			/**
			 * @param field
			 * @param controller
			 */
			self.unregister = function(field, controller){
				sortItems[field].splice(sortItems[field].indexOf(controller), 1);
				if(!sortItems[field].length){
					delete sortItems[field];

				}
				sortItemsId++;
			};
			/**
			 * @param orderValue
			 * @returns {Array}
			 */
			self.getOrder = function(orderValue){
				const order = [],
					parsed = parseOrder(orderValue);
				for(const p in parsed){
					if(parsed.hasOwnProperty(p)){
						order.push({
							col: p,
							dir: parsed[p]
						});
					}
				}
				return order;
			};
			/**
			 * Propagate order change to all child sort directives
			 * @param orderValue
			 */
			self.orderUpdate = function(orderValue){
				if(!angular.equals(orderValue, lastOrder) || lastSortItems !== sortItemsId){
					const parsed = parseOrder(orderValue);
					//
					for(let field in sortItems){
						if(sortItems.hasOwnProperty(field)){
							for(let i = 0; i < sortItems[field].length; i++){
								sortItems[field][i].updateState(
									parsed[field]
								);
							}
						}
					}
					lastOrder = angular.copy(orderValue);
					lastSortItems = sortItemsId;
					return true;
				}
				return false;
			};
		};
	}]);
	/**
	 * @ngdoc directive
	 * @name sort
	 */
	tableTools.directive('sort', [function(){
		return {
			restrict: 'A',
			require: ['^tableTools', 'sort'],
			controller: ['$element', function($element){
				/**
				 * Update sorting item class
				 * @param {string} state
				 */
				this.updateState = function(state){
					if(this.state !== state){
						if(this.state){
							$element.removeClass('sorting-' + this.state);
						}
						if(state){
							$element.addClass('sorting-' + state);
						}
						this.state = state;
					}
				};
			}],
			link(scope, element, attrs, ctrl){
				const tableTools = ctrl[0],
					sortCtrl = ctrl[1];
				//
				tableTools.ttSort.register(attrs['sort'], sortCtrl);
				scope.$on('$destroy', function(){
					tableTools.ttSort.unregister(attrs['sort'], sortCtrl);
				});
				//
				element.on('click', function(e){
					if(!e.shiftKey){ // change sorting direction
						if(tableTools.order === attrs['sort']){
							tableTools.order = '-' + attrs['sort'];
						}else{
							tableTools.order = attrs['sort'];
						}
					}else{ // append to current order array
						if(typeof tableTools.order === 'string'){
							tableTools.order = [tableTools.order];
						}else if(!Array.isArray(tableTools.order)){
							tableTools.order = [];
						}
						let found = false;
						for(let i = 0; i < tableTools.order.length; i++){
							if(tableTools.order[i] === attrs['sort']){
								tableTools.order[i] = '-' + attrs['sort'];
								found = true;
								break;
							}
							if(tableTools.order[i] === '-' + attrs['sort']){
								tableTools.order[i] = attrs['sort'];
								found = true;
								break;
							}
						}
						if(!found){
							tableTools.order.push(attrs['sort']);
						}
					}
					scope.$apply();
				});
			}
		};
	}]);
}();
angular.module('tableTools').run(['$templateCache', function($templateCache) {$templateCache.put('src/templates/export.ng','<div><button class="btn btn-default" ng-click="ttExport.showExport()">{{::tableTools.lang.export}}</button><div class="modal fade" bs-modal="ttExport.modal"><div class="modal-dialog modal-lg"><div class="modal-content"><div class="modal-header"><button type="button" class="close" dismiss aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title">{{::tableTools.lang.export}}</h4></div><div class="modal-body"><div class="form-group"><label>{{::tableTools.lang.exportChooseColumns}}: <a ng-click="ttExport.flipSelection()">[{{::tableTools.lang.flipSelection}}]</a></label><div><label class="checkbox-inline" title="c.txt" ng-repeat="c in ttExport.columns"><input type="checkbox" ng-model="c.exp"> {{::c.txt}}</label></div><div><div class="checkbox"><label><input type="checkbox" ng-model="ttExport.config.columnNames"> {{::tableTools.lang.exportColumnNames}}</label></div></div></div><div class="form-group"><label>{{::tableTools.lang.exportSeparator}}</label><div><label class="radio-inline" ng-repeat="s in ttExport.separators"><input type="radio" ng-model="ttExport.config.separator" ng-value="s.separator"> {{::s.lang}}</label></div></div></div><div class="modal-footer"><button type="button" class="btn btn-default" ng-repeat="(k, e) in ttExport.exportTypes" ng-click="ttExport.doExport(k, e)" ng-disabled="ttExport.exporting">{{::e.lang}} <span ng-if="ttExport.exporting == k"><i class="fa fa-spinner fa-spin"></i></span></button></div></div></div></div></div>');
$templateCache.put('src/templates/pagination.ng','<ng-transclude></ng-transclude><div class="pull-right"><ul class="pagination"><li ng-class="{\'disabled\': tableTools.pagination.page == 1}"><a ng-click="tableTools.changePage(1)" title="{{::tableTools.lang.first}}"><i class="fa fa-angle-double-left"></i></a></li><li ng-class="{\'disabled\': tableTools.pagination.page == 1}"><a ng-click="tableTools.changePage(\'prev\')" title="{{::tableTools.lang.prev}}"><i class="fa fa-angle-left"></i></a></li><li ng-repeat="p in tableTools.pagination.items" ng-class="{\'active\': p == tableTools.pagination.page}"><a ng-click="tableTools.changePage(p)">{{p}}</a></li><li ng-class="{\'disabled\': tableTools.pagination.page == tableTools.pagination.pages || tableTools.pagination.pages == 0}"><a ng-click="tableTools.changePage(\'next\')" title="{{::tableTools.lang.next}}"><i class="fa fa-angle-right"></i></a></li><li ng-class="{\'disabled\': tableTools.pagination.page == tableTools.pagination.pages || tableTools.pagination.pages == 0}"><a ng-click="tableTools.changePage(tableTools.pagination.pages)" title="{{::tableTools.lang.last}}"><i class="fa fa-angle-double-right"></i></a></li></ul></div><div class="pagination-desc">{{::tableTools.lang.results}} <span ng-switch="tableTools.dataLength"><span ng-switch-when="0">0</span> <span ng-switch-default>{{tableTools.pagination.start}} - {{tableTools.pagination.end}} {{::tableTools.lang.from}} {{tableTools.filteredCount}}</span></span> <span ng-show="tableTools.filteredCount !== tableTools.dataLength">({{::tableTools.lang.filteredResults}} {{tableTools.dataLength}})</span></div><div class="clearfix"></div>');}]);